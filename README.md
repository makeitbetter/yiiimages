# yiiimages

[![N|Solid](http://makeitbetter.su/images/projects/yiiimages-xs.png)](http://makeitbetter.su/projects/yiiimages)

Yiiimages is an images manager for Yii framework 2.0. It stores the files in a separate directory. In models images are saved as paths to files relative to this directory. When images is published  their thumbnails are created in an accessible via HTTP location.

## Installation

Add package:
```
#!sh
$ composer require makeitbetter/yiiimages
```

Configure the application by adding ([yii2 guide](http://www.yiiframework.com/doc-2.0/guide-structure-applications.html#modules)):
```
#!php
$config['modules']['yiiimages'] = [
    'class' => 'makeitbetter\yiiimages\Module',
    'options' => [],
];
$config['bootstrap'][] = 'yiiimages';
```
Available options:

- **sourceFolder** - Path to folder  where the images will be stored. Default: `@app/media`.
- **targetFolder** - Path relative to web root where the thumbnails will be putted. Default: *thumbnails*
- **paddingColor** - Hexadecimal RGB color to fill thumbnails padding. Default: white, *ffffff*.
- **getThumbnailPathMethod** - Function to create thumbnail items path. It should takes  *Thumbnail* object and returns string. By default class's self method will be applied.
- **maxUploadNumber** - Max number of images available to be uploaded at one time.

Next options define parameters for create thumbnails in the manager page (see more in the [description](https://bitbucket.org/makeitbetter/thumbnail) of Class *Thumbnail* ):

- **width** - Default: *150*.
- **height** - Default: *150*.
- **crop** - Default: *true*.
- **stretch** - Default: *false*.

Finally, distribute permissions. They should be named: **browseImages**, **addImages**, **deleteImages**.

See more about an authorization in [yii2 guide](http://www.yiiframework.com/doc-2.0/guide-security-authorization.html)


## Usage

####  In you forms:
```
#!php
use makeitbetter\yiiimages\widgets\ImageInput;
use yii\widgets\ActiveForm;

$form = ActiveForm::begin();

echo $form->field($model, 'property_name')->widget(ImageInput::className(), $options);

ActiveForm::end();
```

Or without an *ActiveForm* object:
```
#!php
echo ImageInput::widget(['model' => $model, 'attribute' => 'property_name']);
```

#### Displaying images:

```
#!php
use makeitbetter\thumbnail\Thumbnail;

echo Thumbnail::of($model->propery_name, 150, 150)->img()
```

See more about using a *Thumbnail* helper in the [description](https://bitbucket.org/makeitbetter/thumbnail) of class.

## Internationalisation

For adding translations of module strings register a source `modules/yiiimages`.
For example add to your application config file following code:
```
#!php
$config['components']['i18n'] = [
    'translations' => [
        ...
        'modules*' => [
            'class' => 'yii\i18n\PhpMessageSource',
        ],
    ],
];
```
This configuration means that the application will search the translations at file `@app/messages/xx-XX/modules/yiiimages.php`

See more about internationalisation in [yii2 guide](http://www.yiiframework.com/doc-2.0/guide-tutorial-i18n.html)
