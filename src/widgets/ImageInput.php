<?php

namespace makeitbetter\yiiimages\widgets;

use makeitbetter\yiiimages\Module;
use Yii;
use yii\base\Model;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\InputWidget;

class ImageInput extends InputWidget{

    static protected $css = <<< CSS
        .yiiimages-input {
            position: relative;
        }
        .yiiimages-input > i {
            position: absolute;
            right: 0;
            line-height: 20px;
            padding: 6px 12px;
            cursor: pointer;
        }
CSS;

    static protected $js = <<< JAVASCRIPT
        $(document)
            .on('click', '.yiiimages-input > input', function() {
                var id = this.id;
                if ($(this).val()) {
                    path = $(this).val().split('/');
                    path.pop();
                    path = path.join('/');
                } else {
                    path = localStorage.getItem('yiiimagesLastPath') || '';
                }
                window.open(
                    '/yiiimages/base/index?id=' + id + '&path=' + path,
                    'Yiiimages',
                    'width=1024,height=720,resizable=yes,scrollbars=yes,location=no'
                );
            })
            .on('click', '.yiiimages-input > i', function(e) {
                e.stopPropagation();
                $(this).parent().find('input').val('');
            });
JAVASCRIPT;

    /**
     *
     * @param ActiveForm $form
     * @param Model $model
     * @param string $property
     */
    public function run()
    {
        $id = md5($this->model->className() . ',' . $this->attribute);
        $this->options['id'] = $id;
        if (!isset($this->options['class'])) {
            $this->options['class'] = 'form-control';
        }
        Yii::$app->view->registerCss(static::$css, [], 'yiiimagesInput');
        Yii::$app->view->registerJs(static::$js, View::POS_READY, 'yiiimagesInput');

        return '<div class="yiiimages-input">'
            . Html::activeTextInput($this->model, $this->attribute, $this->options)
            . '<i class="glyphicon glyphicon-remove-circle" title="' . Module::t('Clear') . '"></i>'
            . '</div>';
    }
}