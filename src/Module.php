<?php

namespace makeitbetter\yiiimages;

use makeitbetter\thumbnail\Thumbnail;
use Yii;
use yii\base\InvalidConfigException;

/**
 * yiiimages module definition class
 */
class Module extends yii\base\Module
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'makeitbetter\yiiimages\controllers';

    public $options = [];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        Yii::setAlias('@yiiimages', __DIR__);
        $this->layout = 'index';
        $this->layoutPath = '@yiiimages/layout';

        $defaultOptions = [
            'sourceFolder' => '@app/media',
            'targetFolder' => 'thumbnails',
            'paddingColor' => 'ffffff',
            'getThumbnailPathMethod' => null,
            'width' => 150,
            'height' => 150,
            'crop' => false,
            'stretch' => false,
            'maxUploadNumber' => 10,
        ];
        $this->options = array_replace_recursive($defaultOptions, $this->options);
        $this->options['sourceFolder'] = Yii::getAlias($this->options['sourceFolder']);
        $this->options['targetFolder'] = trim($this->options['targetFolder'], " /\n\t\r");

        Thumbnail::$sourceFolder = $this->options['sourceFolder'];
        Thumbnail::$targetFolder = $this->options['targetFolder'];
        Thumbnail::$webRoot = Yii::getAlias('@webroot');
        Thumbnail::$paddingColor = $this->options['paddingColor'];

        Thumbnail::$getThumbnailPathMethod = $this->options['getThumbnailPathMethod'];
    }

    public static function t($message, $params = [], $language = null)
    {
        try {
            $result = Yii::t('modules/yiiimages', $message, $params, $language);
        } catch (InvalidConfigException $ex ){
            $result = Yii::$app->i18n->format($message, $params, $language);
        }
        return $result;
    }

}
