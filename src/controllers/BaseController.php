<?php

namespace makeitbetter\yiiimages\controllers;

use Exception;
use InvalidArgumentException;
use makeitbetter\yiiimages\models\Folder;
use makeitbetter\yiiimages\Module;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\HttpException;

class BaseController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'create-folder' => ['POST'],
                    'upload' => ['POST'],
                    'group-action' => ['POST'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],

        ];
    }

    public function actionIndex()
    {
        if (!Yii::$app->user->can('browseImages')) {
            throw new HttpException(403, Module::t('You have not permissions to browse images.'));
        }
        $options = $this->module->options;
        $folder = new Folder(Yii::$app->request->get('path'), $options);

        try {
            $data = $folder->read();
        } catch (InvalidArgumentException $e) {
            return $this->render('404', [
                'path' => Yii::$app->request->get('path'),
                'id' => Yii::$app->request->get('id'),
            ]);
        }

        return $this->render('index', [
            'model' => $folder,
            'id' => Yii::$app->request->get('id'),
            'path' => $data['path'],
            'folders' => $data['folders'],
            'images' => $data['images'],
            'canDelete' => Yii::$app->user->can('deleteImages'),
            'canAdd' => Yii::$app->user->can('addImages'),
        ]);
    }

    public function actionUpload()
    {
        if (!Yii::$app->user->can('addImages')) {
            throw new HttpException(403, Module::t('You have not permissions to add images.'));
        }
        $options = $this->module->options;
        $path = Yii::$app->request->post('path');
        $id = Yii::$app->request->post('id');
        $folder = new Folder($path, $options);

        $folder->saveUploadedFiles();

        return $this->redirect('/yiiimages/base/index?path=' . $path . '&id=' . $id);
    }

    public function actionCreateFolder()
    {
        if (!Yii::$app->user->can('addImages')) {
            throw new HttpException(403, Module::t('You have not permissions to add images.'));
        }
        $options = $this->module->options;
        $path = Yii::$app->request->post('path');
        $id = Yii::$app->request->post('id');
        $folder = new Folder($path, $options);
        if (!$folder->createFolder(Yii::$app->request->post('name'))) {
            throw new Exception('Cannot create new folder.');
        };
        return $this->redirect('/yiiimages/base/index?path=' . $path . '&id=' . $id);
    }

    public function actionGroupAction()
    {
        $options = $this->module->options;
        $path = Yii::$app->request->post('path');
        $id = Yii::$app->request->post('id');
        $folder = new Folder($path, $options);
        if (Yii::$app->request->post('delete-selected')) {
            if (!Yii::$app->user->can('deleteImages')) {
                throw new HttpException(403, Module::t('You have not permissions to delete images.'));
            }
            foreach (Yii::$app->request->post('folders', []) as $e) {
                $folder->deleteFolder($e);
            }
            foreach (Yii::$app->request->post('images', []) as $e) {
                $folder->deleteFile($e);
            }
        }
        return $this->redirect('/yiiimages/base/index?path=' . $path . '&id=' . $id);
    }
}
