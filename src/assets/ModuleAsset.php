<?php

namespace makeitbetter\yiiimages\assets;

use yii\web\AssetBundle;

class ModuleAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
