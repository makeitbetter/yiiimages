<?php

namespace makeitbetter\yiiimages\models;

use InvalidArgumentException;
use makeitbetter\yiiimages\Module;
use Yii;
use yii\base\Model;
use yii\caching\FileDependency;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

class Folder extends Model {

    private $strPath = '';
    private $options = null;

    /**
     * @var UploadedFile|Null file attribute
     */
    public $newimages;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'newimages' => Module::t('New Images'),
        ];
    }

    public function __construct($strPath, $options)
    {
        $this->strPath = str_replace('..' , '', trim($strPath, " \n\r\t/."));
        $this->options = $options;
    }

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['newimages'], 'file', 'maxFiles' => $this->options['maxUploadNumber'], 'extensions' => 'gif, jpg, png'],
        ];
    }


    public function read()
    {
        $realPath = $this->options['sourceFolder'] . '/' . $this->strPath;
        $cache = Yii::$app->cache;
        $key = 'yiiimages Folder::read ' . $this->strPath;
        if ($data = $cache->get($key)) {
            return $data;
        }
        $images = [];
        $folders = [];
        if (!is_dir($realPath)) {
            throw new InvalidArgumentException();
        }
        $dir = opendir($realPath);
        while($e = readdir($dir)) {
            if ($e === '.' || $e === '..') {
                continue;
            }
            if (is_dir($realPath . '/' . $e)) {
                $folders[] = [
                    'name' => $e,
                    'path' => $this->strPath . '/' . $e,
                ];
            }
            if (is_file($realPath . '/' . $e)) {
                $mime = mime_content_type($realPath . '/' . $e);
                if (strpos($mime, 'image/') !== 0) {
                    continue;
                }
                $info = getimagesize($realPath . '/' . $e);
                $images[] = [
                    'name' => $e,
                    'path' => $this->strPath . '/' . $e,
                    'size' => filesize($realPath . '/' . $e),
                    'mime' => $mime,
                    'width' => $info[0],
                    'height' => $info[1],
                ];
            }
        }
        $data = [
            'path' => $this->strPath,
            'folders' => $folders,
            'images' => $images
        ];

        $sort = function ($a, $b) {
            return strtolower($a['name']) > strtolower($b['name']) ? 1 : -1;
        };

        usort($data['folders'], $sort);
        usort($data['images'], $sort);

        $dependency = new FileDependency(['fileName' => $realPath]);
        $cache->set($key, $data, null, $dependency);
        return $data;
    }

    public function saveUploadedFiles()
    {
        $this->newimages = UploadedFile::getInstances($this, 'newimages');
        if ($this->newimages && $this->validate()) {
            foreach ($this->newimages as $image) {
                $realPath = $this->options['sourceFolder'] . '/' . $this->strPath . '/' . $image->baseName . '.' . $image->extension;
                $image->saveAs($realPath);
            }
        }
    }

    public function createFolder($name)
    {
        $realPath = $this->options['sourceFolder'] . '/' . $this->strPath;
        $name = Inflector::transliterate($name, 'Any-Latin; Latin-ASCII');
        if (!is_dir($realPath . '/' . $name)) {
            return mkdir($realPath . '/' . $name, 0777, true);
        }
        return false;
    }

    public function deleteFolder($name)
    {
        $dirPath = $this->options['sourceFolder'] . '/' . $this->strPath . '/' . $name;
        $dir = opendir($dirPath);
        $folder = new static($this->strPath . '/' . $name, $this->options);
        while($e = readdir($dir)) {
            if ($e === '.' || $e === '..') {
                continue;
            }
            if (is_dir($dirPath . '/' . $e)) {
                $folder->deleteFolder($e);
            }
            if (is_file($dirPath . '/' . $e)) {
                $folder->deleteFile($e);
            }
        }
        closedir($dir);
        rmdir($dirPath);
    }

    public function deleteFIle($name)
    {
        $filePath = $this->options['sourceFolder'] . '/' . $this->strPath . '/' . $name;
        return unlink($filePath);
    }
}
