<?php

use makeitbetter\yiiimages\Module;

$this->title = 'Yiiimages';
$parts = [];

?>

<ul class="breadcrumb">
    <li>
        <a class="folder" href="?path=&id=<?= $id;?>"><?= Module::t('Images');?></a>
    </li>
    <?php if ($path) : ?>
    <?php foreach (explode('/', $path) as $part) : ?>
    <?php $parts[] = $part;?>
        <li>
            <a href="?path=<?= implode('/', $parts);?>&id=<?= $id;?>"><?= $part;?></a>
        </li>
    <?php endforeach;?>
    <?php endif;?>
</ul>

<div class="alert alert-danger">
    <?= Module::t('Folder not found.');?>
</div>