<?php

use makeitbetter\thumbnail\Thumbnail;
use makeitbetter\yiiimages\Module;
use yii\widgets\ActiveForm;

$this->title = 'Yiiimages';
$this->registerJs(file_get_contents(__DIR__ . '/script.js'));
$this->registerCss(file_get_contents(__DIR__ . '/style.css'));
$parts = [];
$module = $this->context->module;

?>

<ul class="breadcrumb">
    <li>
        <a class="folder" href="?path=&id=<?= $id;?>"><?= Module::t('Images');?></a>
    </li>
    <?php if ($path) : ?>
    <?php foreach (explode('/', $path) as $part) : ?>
    <?php $parts[] = $part;?>
        <li>
            <a href="?path=<?= implode('/', $parts);?>&id=<?= $id;?>"><?= $part;?></a>
        </li>
    <?php endforeach;?>
    <?php endif;?>
</ul>

<div class="row">
    <div class="col-xs-12 col-md-3">
        <?php if ($canAdd) : ?>
        <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'action' => ['upload']]);?>
            <input type="hidden" name="path" value="<?= $path; ?>"/>
            <input type="hidden" name="id" value="<?= $id; ?>"/>
            <p>
                <?= $form->field($model, 'newimages[]')
                    ->fileInput(['multiple' => true])
                    ->hint(Module::t('Maximum {num}', ['num' => $module->options['maxUploadNumber']]));?>
            </p>
            <p>
                <input type="submit" class="btn btn-primary" value="<?= Module::t('Upload');?>"/>
            </p>
        <?php ActiveForm::end() ?>
        <hr/>
        <form action="/yiiimages/base/create-folder" method="post">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
            <input type="hidden" name="path" value="<?= $path; ?>"/>
            <input type="hidden" name="id" value="<?= $id; ?>"/>
            <p class="pull-left">
                <label><?= Module::t('New Folder');?></label>
                <input type="text" name="name" required class="form-control"/>
            </p>
            <p>
                <button type="submit" class="btn btn-primary"><?= Module::t('Create Folder');?></button>
            </p>
        </form>
        <hr/>
        <?php endif;?>

        <?php if ($canDelete): ?>
        <form action="/yiiimages/base/group-action" method="post" id="group-action">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>"/>
            <input type="hidden" name="path" value="<?= $path; ?>"/>
            <input type="hidden" name="id" value="<?= $id; ?>"/>
            <p>
                <input type="submit" class="btn btn-danger" disabled  name="delete-selected" value="<?= Module::t('Delete Selected');?>"/>
            </p>
        </form>
        <?php endif;?>
    </div>

    <div class="col-xs-12 col-md-9">
        <div>
            <?php foreach ($folders as $folder) : ?>
            <big class="item">
                <label class="check-item">
                    <input type="checkbox" name="folders[]" value="<?= $folder['name'];?>" form="group-action"/>
                    <i class="glyphicon text-success"></i>
                    <span>
                        <i class="glyphicon glyphicon-folder-open text-warning"></i>
                    </span>
                </label> &nbsp;
                <a href="?path=<?= $folder['path'];?>&id=<?= $id;?>">
                     <?= $folder['name'];?>
                </a> &nbsp;
                <?php if ($canDelete) : ?>
                <a href="#" title="<?= Module::t('Delete');?>">
                    <i class="glyphicon glyphicon-remove text-danger item-remove"></i>
                </a>
                <?php endif;?>
            </big>
            <?php endforeach;?>
        </div>

        <div>
            <?php foreach ($images as $image) : ?>
            <div class="item">
                <?php if ($id) : ?>
                <a href="#" class="select-item" data-path="<?= $image['path'];?>" data-id="<?= $id;?>">
                <?php endif;?>
                <?= Thumbnail::of(
                        $image['path'],
                        $module->options['width'],
                        $module->options['height'],
                        $module->options['crop'],
                        $module->options['stretch']
                )->img();?>
                <?php if ($id) : ?>
                </a>
                <?php endif;?>
                <p class="thumbnail-filename" style="width: <?= $module->options['width'];?>px;">
                    <?= $image['name'];?>
                </p>
                <p>
                    <?= $image['width'] . ' x ' . $image['height'];?>
                </p>
                <div>
                    <label class="check-item">
                        <input type="checkbox" name="images[]" value="<?= $image['name'];?>" form="group-action"/>
                        <big class="glyphicon text-success"></big>
                    </label>

                    <a href="<?= Thumbnail::of($image['path']);?>" target="_blank" title="View"><big
                            class="glyphicon glyphicon-eye-open text-info"></big></a>
                    <?php if ($canDelete) : ?>
                    <a href="#" title="<?= Module::t('Delete');?>"><big
                            class="glyphicon glyphicon-remove text-danger item-remove"></big></a>
                    <?php endif;?>
                </div>
            </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
