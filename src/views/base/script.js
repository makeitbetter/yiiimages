;(function($) {

    $(document)
            .ready(function() {
                localStorage.setItem('yiiimagesLastPath', $('input[name=path]').eq(0).val() || '');
            })
            .on('click', '.check-item', function() {
                $('#group-action input[type=submit]').prop('disabled', $('.check-item input:checked').length === 0);
            })
            .on('submit', '#group-action', function() {
                return confirm('Are you sure?');
            })
            .on('click', '.item-remove', function() {
                $('.check-item input').prop('checked', false);
                $(this).closest('.item').find('input').prop('checked', true);
                $('#group-action input[name="delete-selected"]').prop('disabled', false).trigger('click');
            })
            .on('click', '.select-item', function() {
                window.opener.$('#' + $(this).data('id')).val($(this).data('path'));
                window.close();
            });
})(jQuery);